const PdfPrinter = require('pdfmake');
const fs = require('fs');

const chartExporter = require("highcharts-export-server");


const fonts = {
	Helvetica: {
		normal: 'Helvetica',
		bold: 'Helvetica-Bold',
		italics: 'Helvetica-Oblique',
		bolditalics: 'Helvetica-BoldOblique'
	}
};


chartExporter.initPool();
const chartDetails = {
	type: "png",
	options: {
		chart: {
			type: "pie"
		},
		title: {
			text: "Heading of Chart"
		},
		plotOptions: {
			pie: {
				dataLabels: {
					enabled: true,
					format: "<b>{point.name}</b>: {point.y}"
				}
			}
		},
		series: [
			{
				data: [
					{
						name: "a",
						y: 100
					},
					{
						name: "b",
						y: 20
					},
					{
						name: "c",
						y: 50
					}
				]
			}
		]
	},
};

chartExporter.export(chartDetails, (err, res) => {
	// Get the image data (base64)
	let imageb64 = res.data;

	// Filename of the output
	let outputFile = "bar.png";

	// // Save the image to file
	// fs.writeFileSync(outputFile, imageb64, "base64", function(err) {
	// 	if (err) console.log(err);
	// });

	console.log("Saved image!");
	chartExporter.killPool();
	const docDefinition = {
		content: [
			'First paragraph',
			'Another paragraph, this time a little bit longer to make sure, this line will be divided into at least two lines',
			{
				image: `data:image/jpeg;base64,${imageb64}`
			}
		  ],
		  defaultStyle: {
			font: 'Helvetica'
		  }
	};
	const printer = new PdfPrinter(fonts);
	const pdfDoc = printer.createPdfKitDocument(docDefinition);
	pdfDoc.pipe(fs.createWriteStream('document.pdf'));
	pdfDoc.end();
	
});